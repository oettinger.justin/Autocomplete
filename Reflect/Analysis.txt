Autocomplete Analysis:
1. Completed.
2. 
Opening - C:\Users\justi\Documents\cs201 Work\201 Assignments\autocompletef17-start-master-a9ad405a3a3965c25a8c19b0535b8edd61a386ab\autocompletef17-start-master-a9ad405a3a3965c25a8c19b0535b8edd61a386ab\data\fourletterwords.txt.
Benchmarking BruteAutocomplete...
Found 456976 words
Time to initialize - 0.061165606
Time for topMatch("") - 0.001960283819
Time for topMatch("nenk") - 0.004929172496
Time for topMatch("n") - 0.002752243444
Time for topMatch("ne") - 0.00283807772
Time for topMatch("notarealword") - 0.006179854364197531
Time for topKMatches("", 1) -  0.00833872013
Time for topKMatches("", 2) -  0.00798285188
Time for topKMatches("", 4) -  0.00802530674
Time for topKMatches("", 8) -  0.00794359862
Time for topKMatches("", 16) -  0.00812380882
Time for topKMatches("", 32) -  0.00809101008
Time for topKMatches("", 64) -  0.00882327786
Time for topKMatches("", 128) -  0.00833887732
Time for topKMatches("", 256) -  0.00839044294
Time for topKMatches("nenk", 1) -  0.00783141435
Time for topKMatches("nenk", 2) -  0.00766899259
Time for topKMatches("nenk", 4) -  0.00783385565
Time for topKMatches("nenk", 8) -  0.00774547165
Time for topKMatches("nenk", 16) -  0.00800399912
Time for topKMatches("nenk", 32) -  0.00782791762
Time for topKMatches("nenk", 64) -  0.00785429391
Time for topKMatches("nenk", 128) -  0.00779927647
Time for topKMatches("nenk", 256) -  0.00790462766
Time for topKMatches("n", 1) -  0.00782135082
Time for topKMatches("n", 2) -  0.00773383297
Time for topKMatches("n", 4) -  0.00751736579
Time for topKMatches("n", 8) -  0.00791380578
Time for topKMatches("n", 16) -  0.00884843191
Time for topKMatches("n", 32) -  0.00845384613
Time for topKMatches("n", 64) -  0.00823512051
Time for topKMatches("n", 128) -  0.00899170504
Time for topKMatches("n", 256) -  0.00915311949
Time for topKMatches("ne", 1) -  0.0096816174
Time for topKMatches("ne", 2) -  0.00852636972
Time for topKMatches("ne", 4) -  0.00797512698
Time for topKMatches("ne", 8) -  0.00806062698
Time for topKMatches("ne", 16) -  0.0083800233
Time for topKMatches("ne", 32) -  0.00797166875
Time for topKMatches("ne", 64) -  0.00766812642
Time for topKMatches("ne", 128) -  0.0085105093
Time for topKMatches("ne", 256) -  0.00817330849
Time for topKMatches("notarealword", 1) -  0.00643277462
Time for topKMatches("notarealword", 2) -  0.00645636635
Time for topKMatches("notarealword", 4) -  0.00726560082
Time for topKMatches("notarealword", 8) -  0.00744636601
Time for topKMatches("notarealword", 16) -  0.0066903525
Time for topKMatches("notarealword", 32) -  0.00636322165
Time for topKMatches("notarealword", 64) -  0.00647033725
Time for topKMatches("notarealword", 128) -  0.00619783247
Time for topKMatches("notarealword", 256) -  0.00679414139

Opening - C:\Users\justi\Documents\cs201 Work\201 Assignments\autocompletef17-start-master-a9ad405a3a3965c25a8c19b0535b8edd61a386ab\autocompletef17-start-master-a9ad405a3a3965c25a8c19b0535b8edd61a386ab\data\fourletterwordshalf.txt.
Benchmarking BruteAutocomplete...
Found 228488 words
Time to initialize - 0.030278844
Time for topMatch("") - 8.72810249E-4
Time for topMatch("aenk") - 0.003836151948
Time for topMatch("a") - 8.5667907E-4
Time for topMatch("ae") - 8.74333094E-4
Time for topMatch("notarealword") - 0.003030870723
Time for topKMatches("", 1) -  0.00398417422
Time for topKMatches("", 2) -  0.00392255789
Time for topKMatches("", 4) -  0.00391853504
Time for topKMatches("", 8) -  0.0038760866
Time for topKMatches("", 16) -  0.00394576146
Time for topKMatches("", 32) -  0.00394344207
Time for topKMatches("", 64) -  0.00401612604
Time for topKMatches("", 128) -  0.00397374817
Time for topKMatches("", 256) -  0.0041456753
Time for topKMatches("aenk", 1) -  0.0038049553
Time for topKMatches("aenk", 2) -  0.00379651822
Time for topKMatches("aenk", 4) -  0.00384926439
Time for topKMatches("aenk", 8) -  0.00381693402
Time for topKMatches("aenk", 16) -  0.0038017601
Time for topKMatches("aenk", 32) -  0.00373336845
Time for topKMatches("aenk", 64) -  0.00378904354
Time for topKMatches("aenk", 128) -  0.00382148298
Time for topKMatches("aenk", 256) -  0.00379164203
Time for topKMatches("a", 1) -  0.00396997234
Time for topKMatches("a", 2) -  0.00378935792
Time for topKMatches("a", 4) -  0.00392510184
Time for topKMatches("a", 8) -  0.00376532028
Time for topKMatches("a", 16) -  0.00376696278
Time for topKMatches("a", 32) -  0.00383470642
Time for topKMatches("a", 64) -  0.00387655496
Time for topKMatches("a", 128) -  0.00386369083
Time for topKMatches("a", 256) -  0.00394976506
Time for topKMatches("ae", 1) -  0.00381314535
Time for topKMatches("ae", 2) -  0.00383412577
Time for topKMatches("ae", 4) -  0.00373873225
Time for topKMatches("ae", 8) -  0.00392611557
Time for topKMatches("ae", 16) -  0.00386178847
Time for topKMatches("ae", 32) -  0.00375194927
Time for topKMatches("ae", 64) -  0.00392030907
Time for topKMatches("ae", 128) -  0.00380943689
Time for topKMatches("ae", 256) -  0.0038104442
Time for topKMatches("notarealword", 1) -  0.00309668346
Time for topKMatches("notarealword", 2) -  0.00315420636
Time for topKMatches("notarealword", 4) -  0.00408980772
Time for topKMatches("notarealword", 8) -  0.00306829253
Time for topKMatches("notarealword", 16) -  0.00313723276
Time for topKMatches("notarealword", 32) -  0.00309661288
Time for topKMatches("notarealword", 64) -  0.00317377846
Time for topKMatches("notarealword", 128) -  0.00300388843
Time for topKMatches("notarealword", 256) -  0.00311835038

Opening - C:\Users\justi\Documents\cs201 Work\201 Assignments\autocompletef17-start-master-a9ad405a3a3965c25a8c19b0535b8edd61a386ab\autocompletef17-start-master-a9ad405a3a3965c25a8c19b0535b8edd61a386ab\data\fourletterwords.txt.
Benchmarking BinarySearchAutocomplete...
Found 456976 words
Time to initialize - 0.06970919
Time for topMatch("") - 0.001871315635
Time for topMatch("nenk") - 5.254086E-6
Time for topMatch("n") - 2.3851908E-5
Time for topMatch("ne") - 2.327414E-6
Time for topMatch("notarealword") - 4.684022E-6
Time for topKMatches("", 1) -  0.00798602781
Time for topKMatches("", 2) -  0.00765127151
Time for topKMatches("", 4) -  0.00760989134
Time for topKMatches("", 8) -  0.00800857053
Time for topKMatches("", 16) -  0.00740228147
Time for topKMatches("", 32) -  0.00783510036
Time for topKMatches("", 64) -  0.00798221028
Time for topKMatches("", 128) -  0.00772374056
Time for topKMatches("", 256) -  0.00763001841
Time for topKMatches("nenk", 1) -  0.00728393782
Time for topKMatches("nenk", 2) -  0.00724081248
Time for topKMatches("nenk", 4) -  0.00730448837
Time for topKMatches("nenk", 8) -  0.00727639578
Time for topKMatches("nenk", 16) -  0.00736624585
Time for topKMatches("nenk", 32) -  0.00723446382
Time for topKMatches("nenk", 64) -  0.00735943844
Time for topKMatches("nenk", 128) -  0.00736721467
Time for topKMatches("nenk", 256) -  0.00733583066
Time for topKMatches("n", 1) -  0.00766312513
Time for topKMatches("n", 2) -  0.00733123037
Time for topKMatches("n", 4) -  0.00780116599
Time for topKMatches("n", 8) -  0.00766876161
Time for topKMatches("n", 16) -  0.00760119761
Time for topKMatches("n", 32) -  0.00737954628
Time for topKMatches("n", 64) -  0.00738316492
Time for topKMatches("n", 128) -  0.00752301189
Time for topKMatches("n", 256) -  0.00760053997
Time for topKMatches("ne", 1) -  0.00730278812
Time for topKMatches("ne", 2) -  0.0073268354
Time for topKMatches("ne", 4) -  0.00725913988
Time for topKMatches("ne", 8) -  0.00734872367
Time for topKMatches("ne", 16) -  0.00716601122
Time for topKMatches("ne", 32) -  0.00736904003
Time for topKMatches("ne", 64) -  0.00726195972
Time for topKMatches("ne", 128) -  0.00726217786
Time for topKMatches("ne", 256) -  0.00736769908
Time for topKMatches("notarealword", 1) -  0.00601170347
Time for topKMatches("notarealword", 2) -  0.00621652878
Time for topKMatches("notarealword", 4) -  0.00597714033
Time for topKMatches("notarealword", 8) -  0.00669839821
Time for topKMatches("notarealword", 16) -  0.00609946513
Time for topKMatches("notarealword", 32) -  0.00599929167
Time for topKMatches("notarealword", 64) -  0.00639234401
Time for topKMatches("notarealword", 128) -  0.00632611455
Time for topKMatches("notarealword", 256) -  0.00623411512

Opening - C:\Users\justi\Documents\cs201 Work\201 Assignments\autocompletef17-start-master-a9ad405a3a3965c25a8c19b0535b8edd61a386ab\autocompletef17-start-master-a9ad405a3a3965c25a8c19b0535b8edd61a386ab\data\fourletterwordshalf.txt.
Benchmarking BinarySearchAutocomplete...
Found 228488 words
Time to initialize - 0.038061807
Time for topMatch("") - 8.62811831E-4
Time for topMatch("aenk") - 3.057879E-6
Time for topMatch("a") - 2.2432683E-5
Time for topMatch("ae") - 3.012326E-6
Time for topMatch("notarealword") - 1.53311E-6
Time for topKMatches("", 1) -  0.00399776979
Time for topKMatches("", 2) -  0.00394281651
Time for topKMatches("", 4) -  0.00393837661
Time for topKMatches("", 8) -  0.00422984718
Time for topKMatches("", 16) -  0.00427380338
Time for topKMatches("", 32) -  0.00431827288
Time for topKMatches("", 64) -  0.00464480704
Time for topKMatches("", 128) -  0.00428778071
Time for topKMatches("", 256) -  0.00443169865
Time for topKMatches("aenk", 1) -  0.00440905007
Time for topKMatches("aenk", 2) -  0.00442388072
Time for topKMatches("aenk", 4) -  0.00444357792
Time for topKMatches("aenk", 8) -  0.00459805024
Time for topKMatches("aenk", 16) -  0.00427975746
Time for topKMatches("aenk", 32) -  0.00440080867
Time for topKMatches("aenk", 64) -  0.00436332624
Time for topKMatches("aenk", 128) -  0.00429643593
Time for topKMatches("aenk", 256) -  0.00434362582
Time for topKMatches("a", 1) -  0.00418716454
Time for topKMatches("a", 2) -  0.00420541814
Time for topKMatches("a", 4) -  0.00430949576
Time for topKMatches("a", 8) -  0.00435620444
Time for topKMatches("a", 16) -  0.00455996469
Time for topKMatches("a", 32) -  0.00431732973
Time for topKMatches("a", 64) -  0.00549483782
Time for topKMatches("a", 128) -  0.00434675684
Time for topKMatches("a", 256) -  0.00436035241
Time for topKMatches("ae", 1) -  0.00403771277
Time for topKMatches("ae", 2) -  0.00411850983
Time for topKMatches("ae", 4) -  0.0041424384
Time for topKMatches("ae", 8) -  0.00415074716
Time for topKMatches("ae", 16) -  0.00411428166
Time for topKMatches("ae", 32) -  0.00415551106
Time for topKMatches("ae", 64) -  0.00423161158
Time for topKMatches("ae", 128) -  0.00417710421
Time for topKMatches("ae", 256) -  0.00433251326
Time for topKMatches("notarealword", 1) -  0.00394842732
Time for topKMatches("notarealword", 2) -  0.00396742839
Time for topKMatches("notarealword", 4) -  0.00405408327
Time for topKMatches("notarealword", 8) -  0.00424196385
Time for topKMatches("notarealword", 16) -  0.00417989518
Time for topKMatches("notarealword", 32) -  0.00423020006
Time for topKMatches("notarealword", 64) -  0.00415447488
Time for topKMatches("notarealword", 128) -  0.0042556332
Time for topKMatches("notarealword", 256) -  0.00412220867

Opening - C:\Users\justi\Documents\cs201 Work\201 Assignments\autocompletef17-start-master-a9ad405a3a3965c25a8c19b0535b8edd61a386ab\autocompletef17-start-master-a9ad405a3a3965c25a8c19b0535b8edd61a386ab\data\fourletterwords.txt.
Benchmarking TrieAutocomplete...
Found 456976 words
Time to initialize - 0.088342621
Created 475255 nodes
Time for topMatch("") - 2.675484E-6
Time for topMatch("nenk") - 1.53663E-7
Time for topMatch("n") - 1.664638E-6
Time for topMatch("ne") - 1.094575E-6
Time for topMatch("notarealword") - 1.5238E-7
Time for topKMatches("", 1) -  0.05250408621
Time for topKMatches("", 2) -  0.05097587156
Time for topKMatches("", 4) -  0.05208019843
Time for topKMatches("", 8) -  0.05362905345
Time for topKMatches("", 16) -  0.05402127812
Time for topKMatches("", 32) -  0.0507246712
Time for topKMatches("", 64) -  0.05396063381
Time for topKMatches("", 128) -  0.05327881527
Time for topKMatches("", 256) -  0.05427587256
Time for topKMatches("nenk", 1) -  2.34827E-6
Time for topKMatches("nenk", 2) -  3.4647E-7
Time for topKMatches("nenk", 4) -  3.4646E-7
Time for topKMatches("nenk", 8) -  3.3363E-7
Time for topKMatches("nenk", 16) -  3.3684E-7
Time for topKMatches("nenk", 32) -  4.3949E-7
Time for topKMatches("nenk", 64) -  3.4005E-7
Time for topKMatches("nenk", 128) -  3.7855E-7
Time for topKMatches("nenk", 256) -  5.1649E-7
Time for topKMatches("n", 1) -  0.00613291188
Time for topKMatches("n", 2) -  0.00609780658
Time for topKMatches("n", 4) -  0.00618540463
Time for topKMatches("n", 8) -  0.00618756362
Time for topKMatches("n", 16) -  0.00611531272
Time for topKMatches("n", 32) -  0.00630317725
Time for topKMatches("n", 64) -  0.00617026601
Time for topKMatches("n", 128) -  0.00613768862
Time for topKMatches("n", 256) -  0.00602682285
Time for topKMatches("ne", 1) -  1.0200844E-4
Time for topKMatches("ne", 2) -  9.608324E-5
Time for topKMatches("ne", 4) -  9.804334E-5
Time for topKMatches("ne", 8) -  9.940033E-5
Time for topKMatches("ne", 16) -  9.829356E-5
Time for topKMatches("ne", 32) -  1.0140854E-4
Time for topKMatches("ne", 64) -  1.0624943E-4
Time for topKMatches("ne", 128) -  1.1384281E-4
Time for topKMatches("ne", 256) -  1.3339245E-4
Time for topKMatches("notarealword", 1) -  1.33774E-6
Time for topKMatches("notarealword", 2) -  6.9613E-7
Time for topKMatches("notarealword", 4) -  1.20301E-6
Time for topKMatches("notarealword", 8) -  4.5233E-7
Time for topKMatches("notarealword", 16) -  4.8441E-7
Time for topKMatches("notarealword", 32) -  4.4271E-7
Time for topKMatches("notarealword", 64) -  4.3309E-7
Time for topKMatches("notarealword", 128) -  4.3308E-7
Time for topKMatches("notarealword", 256) -  4.3308E-7

Opening - C:\Users\justi\Documents\cs201 Work\201 Assignments\autocompletef17-start-master-a9ad405a3a3965c25a8c19b0535b8edd61a386ab\autocompletef17-start-master-a9ad405a3a3965c25a8c19b0535b8edd61a386ab\data\fourletterwordshalf.txt.
Benchmarking TrieAutocomplete...
Found 228488 words
Time to initialize - 0.078449425
Created 237628 nodes
Time for topMatch("") - 2.442903E-6
Time for topMatch("aenk") - 1.54626E-7
Time for topMatch("a") - 1.146865E-6
Time for topMatch("ae") - 1.058323E-6
Time for topMatch("notarealword") - 7.9559E-8
Time for topKMatches("", 1) -  0.05085207749
Time for topKMatches("", 2) -  0.05117452465
Time for topKMatches("", 4) -  0.05050567289
Time for topKMatches("", 8) -  0.05075962252
Time for topKMatches("", 16) -  0.05017713693
Time for topKMatches("", 32) -  0.05244134707
Time for topKMatches("", 64) -  0.05004249977
Time for topKMatches("", 128) -  0.05104418302
Time for topKMatches("", 256) -  0.05166144349
Time for topKMatches("aenk", 1) -  1.76762E-6
Time for topKMatches("aenk", 2) -  5.2933E-7
Time for topKMatches("aenk", 4) -  3.8817E-7
Time for topKMatches("aenk", 8) -  3.9459E-7
Time for topKMatches("aenk", 16) -  4.01E-7
Time for topKMatches("aenk", 32) -  4.7478E-7
Time for topKMatches("aenk", 64) -  3.8175E-7
Time for topKMatches("aenk", 128) -  4.3308E-7
Time for topKMatches("aenk", 256) -  5.4216E-7
Time for topKMatches("a", 1) -  0.00747791683
Time for topKMatches("a", 2) -  0.00666234334
Time for topKMatches("a", 4) -  0.00864820369
Time for topKMatches("a", 8) -  0.00725658308
Time for topKMatches("a", 16) -  0.00720660223
Time for topKMatches("a", 32) -  0.0067209313
Time for topKMatches("a", 64) -  0.00611832504
Time for topKMatches("a", 128) -  0.00626647115
Time for topKMatches("a", 256) -  0.00612513245
Time for topKMatches("ae", 1) -  1.0798176E-4
Time for topKMatches("ae", 2) -  1.1216502E-4
Time for topKMatches("ae", 4) -  1.0190899E-4
Time for topKMatches("ae", 8) -  1.0356112E-4
Time for topKMatches("ae", 16) -  1.1887618E-4
Time for topKMatches("ae", 32) -  1.101953E-4
Time for topKMatches("ae", 64) -  1.1776942E-4
Time for topKMatches("ae", 128) -  1.1828269E-4
Time for topKMatches("ae", 256) -  1.5020886E-4
Time for topKMatches("notarealword", 1) -  1.30567E-6
Time for topKMatches("notarealword", 2) -  4.025096E-5
Time for topKMatches("notarealword", 4) -  4.5553E-7
Time for topKMatches("notarealword", 8) -  2.1494E-7
Time for topKMatches("notarealword", 16) -  2.0531E-7
Time for topKMatches("notarealword", 32) -  2.0852E-7
Time for topKMatches("notarealword", 64) -  1.989E-7
Time for topKMatches("notarealword", 128) -  2.0852E-7
Time for topKMatches("notarealword", 256) -  1.7965E-7


Enjoy.
According to all known laws
of aviation,

  
there is no way a bee
should be able to fly.

  
Its wings are too small to get
its fat little body off the ground.

  
The bee, of course, flies anyway

  
because bees don't care
what humans think is impossible.

  
Yellow, black. Yellow, black.
Yellow, black. Yellow, black.

  
Ooh, black and yellow!
Let's shake it up a little.

  
Barry! Breakfast is ready!

  
Ooming!

  
Hang on a second.

  
Hello?

  
- Barry?
- Adam?

  
- Oan you believe this is happening?
- I can't. I'll pick you up.

  
Looking sharp.

  
Use the stairs. Your father
paid good money for those.

  
Sorry. I'm excited.

  
Here's the graduate.
We're very proud of you, son.

  
A perfect report card, all B's.

  
Very proud.

  
Ma! I got a thing going here.

  
- You got lint on your fuzz.
- Ow! That's me!

  
- Wave to us! We'll be in row 118,000.
- Bye!

  
Barry, I told you,
stop flying in the house!

  
- Hey, Adam.
- Hey, Barry.

  
- Is that fuzz gel?
- A little. Special day, graduation.

  
Never thought I'd make it.

  
Three days grade school,
three days high school.

  
Those were awkward.

  
Three days college. I'm glad I took
a day and hitchhiked around the hive.

3.
Size of prefix affects runtime in order constant time for TrieAutocomplete, in linear time for Binary, and n^2
time for BruteAutocomplete.
Number of matches "k" affects runtimes in constant time for Trie, linear for Binary, and nlogn time for BruteAutocomplete.
